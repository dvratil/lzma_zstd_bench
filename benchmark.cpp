/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <QBuffer>
#include <QDirIterator>
#include <QFile>

#include "compressionstream_p.h"

#include <benchmark/benchmark.h>
#include <string.h>

constexpr auto operator "" _KiB(unsigned long long kb)
{
    return kb * 1024;
}

constexpr auto operator "" _MiB(unsigned long long mb)
{
    return mb * 1024 * 1024;
}

QByteArray generateData(ssize_t size)
{
    // Download the set from here, extract it to /tmp/lzmabench/
    // https://github.com/facebook/zstd/releases/download/v1.1.3/github_users_sample_set.tar.zst

    QByteArray data;

    QDirIterator it(QStringLiteral("/tmp/benchmark"), QDir::Files);
    Q_ASSERT(it.hasNext());
    while (it.hasNext()) {
        it.next();
        QFile f(it.filePath());
        const bool ok = f.open(QIODevice::ReadOnly);
        Q_ASSERT(ok);

        data += f.read(size - data.size());

        if (data.size() == size) {
            return data;
        }
    }

    data += generateData(size - data.size());
    return data;
}

static void BM_compress(benchmark::State &state, std::string func_name)
{
    const auto size = state.range(0);
    const auto level = state.range(1);

    // generate text data of given size, based on zstd benchmark datset (set of Github user API
    // responses in JSON)
    const QByteArray data = generateData(size);

    double ratio = 0;

    for (auto _ : state) {
        state.PauseTiming();
        QBuffer buffer;
        buffer.open(QIODevice::WriteOnly);
        Akonadi::CompressionStream stream(&buffer);
        if (func_name == "lzma") {
            stream.setAlgorithm(Akonadi::CompressionStream::Algorithm::LZMA);
        } else {
            stream.setAlgorithm(Akonadi::CompressionStream::Algorithm::ZSTD);
        }
        stream.setPreset(static_cast<Akonadi::CompressionStream::Preset>(level));
        stream.open(QIODevice::WriteOnly);
        state.ResumeTiming();

        stream.write(data);
        stream.close();

        state.PauseTiming();
        buffer.close();
        ratio = static_cast<double>(data.size()) / buffer.size();
        state.ResumeTiming();
    }

    state.counters["Ratio"] = ratio;
    state.counters["Speed"] = benchmark::Counter(state.range(0), benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1024);
}

void run_test(benchmark::State &state, const std::string &method, const QString &fileName)
{
    const auto level = state.range(0);

    QFile f(fileName);
    const bool ok = f.open(QIODevice::ReadOnly);
    Q_ASSERT(ok);

    const QByteArray data = f.readAll();
    const auto size = data.size();

    double ratio = 0;

    for (auto _ : state) {
        state.PauseTiming();
        QBuffer buffer;
        buffer.open(QIODevice::WriteOnly);
        Akonadi::CompressionStream stream(&buffer);
        if (method == "lzma") {
            stream.setAlgorithm(Akonadi::CompressionStream::Algorithm::LZMA);
        } else {
            stream.setAlgorithm(Akonadi::CompressionStream::Algorithm::ZSTD);
        }
        stream.setPreset(static_cast<Akonadi::CompressionStream::Preset>(level));
        stream.open(QIODevice::WriteOnly);
        state.ResumeTiming();

        stream.write(data);
        stream.close();

        state.PauseTiming();
        buffer.close();
        ratio = static_cast<double>(data.size()) / buffer.size();
        state.ResumeTiming();
    }

    state.counters["Ratio"] = ratio;
    state.counters["Speed"] = benchmark::Counter(state.range(0), benchmark::Counter::kIsIterationInvariantRate, benchmark::Counter::OneK::kIs1024);

}

void generateArgs(benchmark::internal::Benchmark *benchmark)
{
    for (int level = 0; level <= 9; ++level) {
        for (auto size : {1_KiB, 10_KiB, 100_KiB, 1_MiB, 4_MiB, 8_MiB, 10_MiB, 15_MiB, 20_MiB}) {
            benchmark->Args({static_cast<long>(size), level});
        }
    }
}

int main(int argc, char **argv)
{
    bool customData = false;

    for (int i = 0; i < argc; ++i) {
        if (strlen(argv[i]) == 2 && strncmp(argv[i], "-d", 2) == 0) {
            customData = true;
            break;
        }
    }

    if (customData) {
        QDirIterator it(QStringLiteral("/tmp/benchmark_data"), QDir::Files);
        Q_ASSERT(it.hasNext());
        while (it.hasNext()) {
            it.next();
            benchmark::RegisterBenchmark(qUtf8Printable(QStringLiteral("lzma/%1").arg(it.fileName())), run_test, "lzma", it.filePath())
                ->DenseRange(0, 9)
                ->Unit(benchmark::kMillisecond);
            benchmark::RegisterBenchmark(qUtf8Printable(QStringLiteral("zstd/%1").arg(it.fileName())), run_test, "zstd", it.filePath())
                ->DenseRange(0, 9)
                ->Unit(benchmark::kMillisecond);
        }
    } else {
        benchmark::RegisterBenchmark("BM_compress/lzma", BM_compress, "lzma")
            ->Apply(generateArgs)
            ->Unit(benchmark::kMillisecond);

        benchmark::RegisterBenchmark("BM_compress/zstd", BM_compress, "zstd")
            ->Apply(generateArgs)
            ->Unit(benchmark::kMillisecond);
    }


    benchmark::Initialize(&argc, argv);
    benchmark::RunSpecifiedBenchmarks();
}
