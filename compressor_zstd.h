/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "compressor_p.h"

#include <QByteArray>

#include <memory>
#include <system_error>

#include <zstd.h>
#include <zstd_errors.h>

namespace Akonadi
{

class ZSTDErrorCategory : public std::error_category
{
public:
    const char *name() const noexcept override;
    std::string message(int ev) const noexcept override;
};

} // namespace Akonadi

namespace std
{

template<> struct is_error_code_enum<ZSTD_ErrorCode> : std::true_type {};
std::error_condition make_error_condition(ZSTD_ErrorCode ret);

} // namespace std

std::error_code make_error_code(ZSTD_ErrorCode e);


namespace Akonadi
{

class ZSTDCompressor : public Compressor
{
public:
    explicit ZSTDCompressor(QIODevice *device);

    std::error_code initialize(QIODevice::OpenMode openMode, CompressionStream::Preset preset) override;

    std::error_code finalize() override;
    std::error_code error() const override;
    bool atEnd() const override;

    qint64 read(char *data, qint64 dataSize) override;
    qint64 write(const char *data, qint64 dataSize) override;

protected:
    void setInputBuffer(const char *data, qint64 size);
    void setOutputBuffer(char *data, qint64 maxSize);

    qint64 writeBuffer(bool finish);

    struct StreamFree {
        void operator()(ZSTD_CStream *str) {
            ZSTD_freeCStream(str);
        }
        void operator()(ZSTD_DStream *str) {
            ZSTD_freeDStream(str);
        }
    };

    std::unique_ptr<ZSTD_CStream, StreamFree> mCStream;
    std::unique_ptr<ZSTD_DStream, StreamFree> mDStream;
    ZSTD_inBuffer_s mInBuffer;
    ZSTD_outBuffer_s mOutBuffer;
    QByteArray mBuffer;
    std::error_code mResult = ZSTD_error_no_error;
    QIODevice *mDevice = nullptr;
    bool mAtEnd = false;
};

} // namespace Akonadi

