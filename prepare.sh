#!/bin/sh

cd /tmp

if ! command -v zstd &> /dev/null; then
    echo "ZSTD compression utility is missing"
    exit 2
fi

if [ -d benchmark ]; then
    echo "/tmp/benchmark directory already exists"
    exit 2
fi

wget https://github.com/facebook/zstd/releases/download/v1.1.3/github_users_sample_set.tar.zst -O /tmp/github_users_sample_set.tar.zst
zstd -d github_users_sample_set.tar.zst
tar -xf github_users_sample_set.tar
mv github benchmark
