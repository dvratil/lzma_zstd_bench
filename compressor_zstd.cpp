/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "compressor_zstd.h"

#include <QDebug>

using namespace Akonadi;

const char *ZSTDErrorCategory::name() const noexcept { return "zstd"; }
std::string ZSTDErrorCategory::message(int ev) const noexcept
{
    return ZSTD_getErrorString(static_cast<ZSTD_ErrorCode>(ev));
}

namespace
{

const ZSTDErrorCategory &zstdErrorCategory()
{
    static const ZSTDErrorCategory zstdErrorCategory {};
    return zstdErrorCategory;
}

} // namespace

namespace std
{

std::error_condition make_error_condition(ZSTD_ErrorCode ret)
{
    return std::error_condition(static_cast<ZSTD_ErrorCode>(ret), zstdErrorCategory());
}

} // namespace std

std::error_code make_error_code(ZSTD_ErrorCode e)
{
    return {static_cast<ZSTD_ErrorCode>(e), zstdErrorCategory()};
}

ZSTDCompressor::ZSTDCompressor(QIODevice *device)
    : mDevice(device)
{}

std::error_code ZSTDCompressor::error() const
{
    return mResult;
}

bool ZSTDCompressor::atEnd() const
{
    return mAtEnd;
}

std::error_code ZSTDCompressor::initialize(QIODevice::OpenMode openMode, CompressionStream::Preset preset)
{
    if (openMode == QIODevice::ReadOnly) {
        mDStream.reset(ZSTD_createDStream());
        ZSTD_initDStream(mDStream.get());
    } else {
        mBuffer.resize(ZSTD_CStreamOutSize());
        setOutputBuffer(mBuffer.data(), mBuffer.size());

        mCStream.reset(ZSTD_createCStream());
        ZSTD_initCStream(mCStream.get(), static_cast<int>(preset));
    }

    return ZSTD_error_no_error;
}

void ZSTDCompressor::setInputBuffer(const char *data, qint64 size)
{
    mInBuffer.src = data;
    mInBuffer.size = size;
    mInBuffer.pos = 0;
}

void ZSTDCompressor::setOutputBuffer(char *data, qint64 maxSize)
{
    mOutBuffer.dst = data;
    mOutBuffer.size = maxSize;
    mOutBuffer.pos = 0;
}

std::error_code ZSTDCompressor::finalize()
{
    if (mCStream) {
        write(nullptr, 0);
        mAtEnd = true;
    }

    return ZSTD_error_no_error;
}

qint64 ZSTDCompressor::read(char *data, qint64 dataSize)
{
    setOutputBuffer(data, dataSize);

    while (mOutBuffer.pos < mOutBuffer.size && !mDevice->atEnd()) {
        if (mInBuffer.pos == mInBuffer.size || mInBuffer.size == 0) {
            mBuffer.resize(ZSTD_DStreamOutSize());
            const auto len = mDevice->read(mBuffer.data(), mBuffer.size());
            setInputBuffer(mBuffer.data(), len);
        }

        const int result = ZSTD_decompressStream(mDStream.get(), &mOutBuffer, &mInBuffer);
        if (ZSTD_isError(result)) {
            mResult = ZSTD_getErrorCode(result);
            qWarning() << "Error while decompressing ZSTD stream:" << mResult.message();
            return -1;
        } else if (result == 0) {
            mAtEnd = true;
            break;
        }
    }

    return mOutBuffer.pos;
}

qint64 ZSTDCompressor::write(const char *data, qint64 dataSize)
{

    if (data == nullptr) {
        int result = 0;
        Q_FOREVER {
            result = ZSTD_compressStream2(mCStream.get(), &mOutBuffer, &mInBuffer, ZSTD_e_end);
            if (ZSTD_isError(result)) {
                mResult = ZSTD_getErrorCode(result);
                qWarning() << "Failed to flush ZSTD stream:" << mResult.message();
                return -1;
            }

            mDevice->write(static_cast<const char *>(mOutBuffer.dst), mOutBuffer.pos);

            if (result > 0) {
                mBuffer.resize(result);
                setOutputBuffer(mBuffer.data(), mBuffer.size());
            } else {
                return 0;
            }
        }
    }

    setInputBuffer(data, dataSize);

    int size = 0;
    while (mInBuffer.pos < mInBuffer.size) {
        if (mOutBuffer.pos == mOutBuffer.size) {
            mDevice->write(static_cast<const char *>(mOutBuffer.dst), mOutBuffer.pos);
            mBuffer.resize(ZSTD_DStreamOutSize());
            mOutBuffer.pos = 0;
        }

        const int oldPos = mInBuffer.pos;
        const auto result = ZSTD_compressStream2(mCStream.get(), &mOutBuffer, &mInBuffer, ZSTD_e_continue);
        if (ZSTD_isError(result)) {
            mResult = ZSTD_getErrorCode(result);
            qWarning() << "Error while compressing ZSTD stream:" << mResult.message();
            return -1;
        }

        size += (mInBuffer.pos - oldPos);
    }

    return size;
}
