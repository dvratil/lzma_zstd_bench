/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "compressor_lzma.h"

#include <QDebug>

using namespace Akonadi;

const char *LZMAErrorCategory::name() const noexcept { return "lzma"; }
std::string LZMAErrorCategory::message(int ev) const noexcept
{
    switch (static_cast<lzma_ret>(ev)) {
    case LZMA_OK: return "Operation completed succesfully";
    case LZMA_STREAM_END: return "End of stream was reached";
    case LZMA_NO_CHECK: return "Input stream has no integrity check";
    case LZMA_UNSUPPORTED_CHECK: return "Cannot calculate the integrity check";
    case LZMA_GET_CHECK: return "Integrity check type is now available";
    case LZMA_MEM_ERROR: return "Cannot allocate memory";
    case LZMA_MEMLIMIT_ERROR: return "Memory usage limit was reached";
    case LZMA_FORMAT_ERROR: return "File format not recognized";
    case LZMA_OPTIONS_ERROR: return "Invalid or unsupported options";
    case LZMA_DATA_ERROR: return "Data is corrupt";
    case LZMA_BUF_ERROR: return "No progress is possible";
    case LZMA_PROG_ERROR: return "Programming error";
    }

    Q_UNREACHABLE();
}

namespace
{

const LZMAErrorCategory &lzmaErrorCategory()
{
    static const LZMAErrorCategory lzmaErrorCategory {};
    return lzmaErrorCategory;
}

} // namespace

namespace std
{

std::error_condition make_error_condition(lzma_ret ret)
{
    return std::error_condition(static_cast<int>(ret), lzmaErrorCategory());
}

} // namespace std

std::error_code make_error_code(lzma_ret e)
{
    return {static_cast<int>(e), lzmaErrorCategory()};
}

LZMACompressor::LZMACompressor(QIODevice *device)
    : mDevice(device)
{}

std::error_code LZMACompressor::initialize(QIODevice::OpenMode openMode, CompressionStream::Preset preset)
{
    if (openMode == QIODevice::ReadOnly) {
        return lzma_auto_decoder(&mStream, 100 * 1024 * 1024 /* 100 MiB */, 0);
    } else {
        mBuffer.resize(BUFSIZ);
        setOutputBuffer(mBuffer.data(), mBuffer.size());

        return lzma_easy_encoder(&mStream, static_cast<int>(preset), LZMA_CHECK_CRC32);
    }
}

std::error_code LZMACompressor::error() const
{
    return mResult == LZMA_STREAM_END ? LZMA_OK : mResult;
}

bool LZMACompressor::atEnd() const
{
    return mResult == LZMA_STREAM_END;
}

void LZMACompressor::setInputBuffer(const char *data, qint64 size)
{
    mStream.next_in = reinterpret_cast<const uint8_t *>(data);
    mStream.avail_in = size;
}

void LZMACompressor::setOutputBuffer(char *data, qint64 maxSize)
{
    mStream.next_out = reinterpret_cast<uint8_t *>(data);
    mStream.avail_out = maxSize;
}

std::error_code LZMACompressor::finalize()
{
    write(nullptr, 0);
    lzma_end(&mStream);
    return LZMA_OK;
}

qint64 LZMACompressor::read(char *data, qint64 dataSize)
{
    qint64 dataRead = 0;

    if (mResult == LZMA_STREAM_END) {
        return 0;
    } else if (mResult != LZMA_OK) {
        return -1;
    }

    setOutputBuffer(data, dataSize);

    while (dataSize > 0) {
        if (mStream.avail_in == 0) {
            mBuffer.resize(BUFSIZ);
            const auto compressedDataRead = mDevice->read(mBuffer.data(), mBuffer.size());

            if (compressedDataRead > 0) {
                setInputBuffer(mBuffer.data(), compressedDataRead);
            } else {
                break;
            }
        }

        mResult = lzma_code(&mStream, LZMA_RUN);

        if (mResult != LZMA_OK && mResult != LZMA_STREAM_END) {
            qWarning() << "Error while decompressing LZMA stream:" << mResult.message();
            break;
        }

        const auto decompressedDataRead = dataSize - mStream.avail_out;
        dataRead += decompressedDataRead;
        dataSize -= decompressedDataRead;

        if (mResult == LZMA_STREAM_END) {
            if (mDevice->atEnd()) {
                break;
            }
        }

        setOutputBuffer(data + dataRead, dataSize);
    }

    return dataRead;
}

qint64 LZMACompressor::write(const char *data, qint64 dataSize)
{
    if (mResult != LZMA_OK) {
        return 0;
    }

    bool finish = (data == nullptr);
    if (!finish) {
        setInputBuffer(data, dataSize);
    }

    qint64 dataWritten = 0;

    while (dataSize > 0 || finish) {
        mResult = lzma_code(&mStream, finish ? LZMA_FINISH : LZMA_RUN);

        if (mResult != LZMA_OK && mResult != LZMA_STREAM_END) {
            qWarning() << "Error while compressing LZMA stream:" << mResult.message();
            break;
        }

        if (mStream.avail_in == 0 || (mResult == LZMA_STREAM_END)) {
            const auto wrote = dataSize - mStream.avail_in;

            dataWritten += wrote;
            dataSize -= wrote;

            if (dataSize > 0) {
                setInputBuffer(data + dataWritten, dataSize);
            }
        }

        if (mStream.avail_out == 0 || (mResult == LZMA_STREAM_END) || finish) {
            const auto toWrite = mBuffer.size() - mStream.avail_out;
            if (toWrite > 0) {
                const auto writtenSize = mDevice->write(mBuffer.constData(), toWrite);
                if (writtenSize != toWrite) {
                    qWarning() << "Failed to write compressed data to output device:" << mDevice->errorString();
                    return 0;
                }
            }

            if (mResult == LZMA_STREAM_END) {
                Q_ASSERT(finish);
                break;
            }
            mBuffer.resize(BUFSIZ);
            setOutputBuffer(mBuffer.data(), mBuffer.size());
        }
    }

    return dataWritten;
}

