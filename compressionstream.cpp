/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "compressionstream_p.h"
#include "compressor_lzma.h"
#include "compressor_zstd.h"

#include <QByteArray>
#include <QDebug>

#include <array>

using namespace Akonadi;

namespace std
{

QDebug operator<<(QDebug dbg, const std::string &str)
{
    dbg << QString::fromStdString(str);
    return dbg;
}

} // namespace std


CompressionStream::CompressionStream(QIODevice *stream, QObject *parent)
    : QIODevice(parent)
    , mStream(stream)
{}

CompressionStream::~CompressionStream()
{
    CompressionStream::close();
}

void CompressionStream::setPreset(Preset preset)
{
    mPreset = preset;
}

void CompressionStream::setAlgorithm(Algorithm algorithm)
{
    mAlgorithm = algorithm;
}

bool CompressionStream::open(OpenMode mode)
{
    if ((mode & QIODevice::ReadOnly) && (mode & QIODevice::WriteOnly)) {
        qWarning() << "Invalid open mode for CompressionStream.";
        return false;
    }

    switch (mAlgorithm) {
    case Algorithm::LZMA:
        mCompressor.reset(new LZMACompressor{mStream});
        break;
    case Algorithm::ZSTD:
        mCompressor.reset(new ZSTDCompressor{mStream});
        break;
    }

    if (const auto err = mCompressor->initialize(mode & QIODevice::ReadOnly ? QIODevice::ReadOnly : QIODevice::WriteOnly, mPreset); err) {
        qWarning() << "Failed to initialize stream coder:" << err.message();
        return false;
    }

    return QIODevice::open(mode);
}

void CompressionStream::close()
{
    if (!isOpen()) {
        return;
    }

    mCompressor->finalize();

    setOpenMode(QIODevice::NotOpen);
}

std::error_code CompressionStream::error() const
{
    return mCompressor->error();
}

bool CompressionStream::atEnd() const
{
    return mCompressor->atEnd() && QIODevice::atEnd() && mStream->atEnd();
}

qint64 CompressionStream::readData(char *data, qint64 dataSize)
{
    return mCompressor->read(data, dataSize);
}

qint64 CompressionStream::writeData(const char *data, qint64 dataSize)
{
    return mCompressor->write(data, dataSize);
}

bool CompressionStream::isCompressed(QIODevice *data)
{
    constexpr std::array<uchar, 6> magic = {0xfd, 0x37, 0x7a, 0x58, 0x5a, 0x00};

    if (!data->isOpen() && !data->isReadable()) {
        return false;
    }

    char buf[6] = {};
    if (data->peek(buf, sizeof(buf)) != sizeof(buf)) {
        return false;
    }

    return memcmp(magic.data(), buf, sizeof(buf)) == 0;
}

