/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "compressionstream_p.h"

#include <system_error>

class QIODevice;

namespace Akonadi
{

class Compressor {
public:
    virtual ~Compressor() = default;
    virtual std::error_code initialize(QIODevice::OpenMode openMode, CompressionStream::Preset
    preset) = 0;

    virtual std::error_code error() const = 0;
    virtual bool atEnd() const = 0;

    virtual std::error_code finalize() = 0;

    virtual qint64 read(char *data, qint64 dataSize) = 0;
    virtual qint64 write(const char *data, qint64 len) = 0;

};

} // namespace Akonadi


