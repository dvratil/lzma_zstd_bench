# Dependencies

* liblzma (https://tukaani.org/xz/)
* libzstd (https://github.com/facebook/zstd)
* benchmark (https://github.com/google/benchmark)
* Qt5Core

# Compilation

```
mkdir build
cd build
cmake ..
make -j5
```

# Running

```
# Download benchmark data
./prepare.sh

# Run the benchmark
./build/lzma_zstd_bench
```
