/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include "compressor_p.h"

#include <QByteArray>

#include <system_error>

#include <lzma.h>

namespace Akonadi
{

class LZMAErrorCategory : public std::error_category
{
public:
    const char *name() const noexcept override;
    std::string message(int ev) const noexcept override;
};

} // namespace Akonadi

namespace std
{

template<> struct is_error_code_enum<lzma_ret> : std::true_type {};
std::error_condition make_error_condition(lzma_ret ret);

} // namespace std

std::error_code make_error_code(lzma_ret e);

namespace Akonadi
{

class LZMACompressor : public Compressor
{
public:
    explicit LZMACompressor(QIODevice *device);

    std::error_code initialize(QIODevice::OpenMode openMode, CompressionStream::Preset preset) override;

    std::error_code error() const override;
    bool atEnd() const override;

    qint64 read(char *data, qint64 dataSize) override;
    qint64 write(const char *data, qint64 dataSize) override;

    std::error_code finalize() override;

private:
    void setInputBuffer(const char *data, qint64 size);
    void setOutputBuffer(char *data, qint64 maxSize);

protected:
    QByteArray mBuffer;
    lzma_stream mStream = LZMA_STREAM_INIT;
    std::error_code mResult = LZMA_OK;
    QIODevice *mDevice = nullptr;
};

} // namespace Akonadi

