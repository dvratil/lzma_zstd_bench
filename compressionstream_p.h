/*
    SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef AKONADI_COMPRESSIONSTREAM_H_
#define AKONADI_COMPRESSIONSTREAM_H_

#include <QIODevice>

#include <memory>
#include <system_error>

namespace Akonadi
{

class Compressor;
class CompressionStream : public QIODevice
{
    Q_OBJECT
public:
    enum class Algorithm {
        LZMA,
        ZSTD
    };

    enum class Preset {
       Preset_0 = 0,
       Preset_1,
       Preset_2,
       Preset_3,
       Preset_4,
       Preset_5,
       Preset_6,
       Preset_7,
       Preset_8,
       Preset_9,

       Fastest = Preset_0,
       Default = Preset_6,
       Best = Preset_9
    };

    explicit CompressionStream(QIODevice *stream, QObject *parent = nullptr);
    ~CompressionStream() override;

    void setAlgorithm(Algorithm algorithm);
    void setPreset(Preset preset);

    bool open(QIODevice::OpenMode mode) override;
    void close() override;
    bool atEnd() const override;

    std::error_code error() const;

    static bool isCompressed(QIODevice *data);

protected:
    qint64 readData(char *data, qint64 maxSize) override;
    qint64 writeData(const char *data, qint64 maxSize) override;

private:
    QIODevice *mStream = nullptr;
    std::unique_ptr<Compressor> mCompressor;

    Algorithm mAlgorithm = Algorithm::LZMA;
    Preset mPreset = Preset::Default;
};

} // namespace Akonadi

namespace std
{
QDebug operator<<(QDebug dbg, const std::string &str);
}

#endif

